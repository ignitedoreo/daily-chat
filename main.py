import gd, nest_asyncio
import dearpygui.dearpygui as dpg
nest_asyncio.apply()
client = gd.Client()

dpg.create_context()
dpg.create_viewport(title='DailyChat', width=910, height=400, resizable=False)
dpg.setup_dearpygui()

daily = "Click Update!"
counter = 0

def send_message(sender, data):
	if client.account_id != 0:
		client.run(sendmsg())
		dpg.set_value("daily-input", "")
	else:
		dpg.configure_item("err", show=True)


async def sendmsg():
	await daily.comment(dpg.get_value("daily-input"))

def update():
	dpg.set_value("daily-chat", "")
	dpg.set_value("daily-chat", client.run(upd()))

async def upd():
	help = ""
	for comment in await daily.get_comments():
		help = "\n" + str(comment.author) + ": " + str(comment.body) + help
	help = help[1:]
	return help

async def forceupdateasync():
	help = ""
	for comment in await daily.get_comments():
		help = "\n" + str(comment.author) + ": " + str(comment.body) + help
	help = help[1:]
	return help

def try_to_log_in(sender, data):
	client.run(login())

def set_daily(sender, data):
	global daily
	dpg.set_value("daily-text", "Wait a minute...")
	daily = client.run(get_daily())
	dpg.set_value("daily-text", "Current daily:\n" + str(daily.id))

async def login():
	dpg.set_value("login_text", "Wait a minute...")
	try:
		client.close()
		await client.login(dpg.get_value("username"), dpg.get_value("password"))
		dpg.set_value("login_text", "Succefly logged-in!")
	except:
		dpg.set_value("login_text", "Wrong credentials!")
async def get_daily():
	daily = await client.get_daily()
	return daily

with dpg.window(label="Error!", show=False, pos=(430, 380), tag="err"):
	dpg.add_text("You must be logged-in to do that!")
	dpg.add_button(label="Close", callback=lambda: dpg.configure_item("err", show=False))

with dpg.window(label="Login", no_close=True, no_resize=True, height=100, width=220, pos=(10, 10), no_move=True, no_collapse=True):
	dpg.add_input_text(label="Username", tag="username")
	dpg.add_input_text(label="Password", tag="password")
	with dpg.group(horizontal=True):
		dpg.add_button(label="Log-in", tag="login_button", callback=try_to_log_in)
		dpg.add_text("Log-in first!", tag="login_text")

with dpg.window(label="Chat", no_close=True, no_resize=True, width=600, height=330, pos=(240, 10), no_move=True, no_collapse=True):
	dpg.add_input_text(multiline=True, readonly=True, width=584, height=270, tag="daily-chat")
	with dpg.group(horizontal=True):
		dpg.add_input_text(width=504, tag="daily-input")
		dpg.add_button(label="Send!", width=72, callback=send_message)

with dpg.window(label="Update", no_close=True, no_resize=True, pos=(10, 120), no_move=True, no_collapse=True):
	dpg.add_button(label="Update", width=110, height=100, callback=set_daily)
	dpg.add_input_text(default_value="Current daily:\n" + daily, multiline=True, readonly=True, height=32, width=110, tag="daily-text")

dpg.show_viewport()

while dpg.is_dearpygui_running():
    if daily != "Click Update!":
    	if counter > 50:
    		update()
    		counter = 0
    dpg.render_dearpygui_frame()
    counter = counter + 1

dpg.destroy_context()